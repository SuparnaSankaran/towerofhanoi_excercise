
/**
 * Author: Suparna Sankaran
 * 
 * Tower of Hanoi using iteration.
 */

package hanoi;

import java.util.InputMismatchException;
import java.util.Scanner;



public class Tower {
	
	static Stack sourceStack;		//source stack
	static Stack auxiliaryStack;	//auxiliary stack
	static Stack destinationStack;	//Destination stack
	
	//Move n discs from source stack to destination stack and print the moves.
	private static void hanoiIterative(final int n){
		
		int totalmoves=(int)(java.lang.Math.pow(2,n)-1);
		
		//If n is an even number, the auxiliary and destination stacks are interchanged.
		if(n%2==0){
			Stack temp;
			temp=auxiliaryStack;
			auxiliaryStack=destinationStack;
			destinationStack=temp;	
		}

		for (int i=0;i<totalmoves;i++){
						
			switch(i%3){
				case 0:
					//move disk from source to destination(n is odd), auxiliary(n is even)
					move(sourceStack,destinationStack);
				break;
			
				case 1:
					//move disk from source to auxiliary(n is odd), destination(n is even)
					move(sourceStack,auxiliaryStack);
					break;
			
				case 2:
					//move disk from auxiliary to destination(n is odd), or vice versa (n is even)
					move(auxiliaryStack,destinationStack);
					break;				
				default:
					break;
			}
		}
	}
	
	//Check the Stack top value and index and move one disc at a time based on the legal moves.
	private static void move(Stack src, Stack dest){
		int srcTopValue=src.popStack();
		int destTopValue=dest.popStack();
		
		//If source is empty, move disc from destination to source.
		if(srcTopValue==-1){
			src.pushStack(destTopValue);
			System.out.println("Move from "+dest.getType()+" to "+src.getType());
		}
		//If destination is empty, move disc from source to destination.
		else if(destTopValue==-1){	
			dest.pushStack(srcTopValue);
			System.out.println("Move from "+src.getType()+" to "+dest.getType());
		}
		//If the disc on the top of source is bigger that on destination, move from destination to source.
		else if(srcTopValue>destTopValue){
			src.pushStack(srcTopValue);
			dest.pushStack(destTopValue);
			src.pushStack(dest.popStack());
			System.out.println("Move from "+dest.getType()+" to "+src.getType());
		}
		//If the disc on the top of destination is bigger that on source, move from source to destination.
		else {
			src.pushStack(srcTopValue);
			dest.pushStack(destTopValue);
			dest.pushStack(src.popStack());
			System.out.println("Move from "+src.getType()+" to "+dest.getType());
		}		
	}
	
	

	public static void main(String args[]){
		//Number of discs
		int n=0;
		
		//Read the value n.
		Scanner scanner = new Scanner(System. in); 
		System.out.println("TOWER Of Hanoi: Enter the number of disks to be moved");
		
		
		try{
			//Validate the value of n
			n= scanner.nextInt();
			if(n<=0||n>32)
				throw new NumberFormatException();
		
			//Allocate memory and initialize the stacks.
			sourceStack=new Stack(n,'1'); 		//source
			auxiliaryStack=new Stack(n,'2'); 	//auxiliary
			destinationStack=new Stack(n,'3');	//destination 
			for(int i=0;i<n;i++){
				sourceStack.pushStack(n-i);
			}
			
			//Move the disks from source tower to destination tower.
			hanoiIterative(n);
		}
		catch(NumberFormatException e){
			System.out.println("Not a valid integer (1 to 32 are valid)");
		}
		catch(InputMismatchException e){ 
			System.out.println("Not a valid integer (1 to 32 are valid)");
		}
		catch(Exception e){
			System.out.println(e);
		}	
	}
}
