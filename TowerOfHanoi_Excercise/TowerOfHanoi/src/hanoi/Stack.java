/**
 * Author: Suparna Sankaran
 * 
 * Tower of Hanoi using iteration.
 */


package hanoi;

//Discs in a tower manipulated using stacks.
public class Stack {

	 private int top;				//keeps track of the index for top of stack.
	 private int content[];			//Disc represented as numbers stored in this array.
	 private char type;				//Indicates the type of the tower: Source(1), destination(3) or Auxiliary(2)
	
	//Constructor
	Stack(int maxDiscs,char type){ 

		this.top=-1;
		this.content=new int[maxDiscs];
		this.type=type;
	}
	
	//Constructor
	Stack(){ 
		this.top=-1;
		this.content=new int[0];
	}
	
	//Check if the stack is full. Returns true if the stack is full. Else false.
	boolean isStackFull(){
		return (top==(content.length-1));	
	}
	
	//Check if the stack is empty. Returns true if the stack is empty. Else false.
	boolean isStackEmpty(){
		return (top<0);		//Stack underflow
	}
	
	//Add the value n, to the top of stack if the stack is not full.
	void pushStack(int n){
		if(isStackFull())
			return;		//Stack overflow
		else 
			content[++top]=n;
	}
	
	//Pop from the stack and return the top most value in the stack if the stack is not empty.
	int popStack(){
		if(isStackEmpty())
			return -1;
		else 
			return(content[top--]);
	}
	
	//Set the type of the Stack.
	void setType(char type){
		this.type=type;
	}
	
	//Returns the type of the Stack.
	char getType(){
		return this.type;
	}
}
